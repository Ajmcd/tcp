$(function(){
	var controller = new ScrollMagic.Controller();


// Grouping
// ------------------------------------------------------
	var groupingTimeline = new TimelineMax ()
	.add([
		TweenMax.from('.bulbs_bulb-even', 1, {y: 100, opacity: 0}),
		TweenMax.from('.bulbs_bulb-odd', 1, {y: -100, opacity: 0}),
		TweenMax.from('.bulb-lit', 1, {opacity: 0}),
		TweenMax.from('.grouping .headline-main', .75, {delay: 1, y: 150, opacity: 0}),
		TweenMax.to('.bulb-lit', 1, {delay: 1, opacity: .3}),
		TweenMax.to('.bulb-lit', 1, { opacity: 1}),
		TweenMax.to('.bulb-unlit', 1, {opacity: 0})
	])

	var bulbs = new ScrollMagic.Scene( {
		triggerElement: '#groupingWrapper',
		triggerHook: 0,
		duration: '100%',
	})
	.setTween(groupingTimeline)
	.setPin('#groupingWrapper')
	// .addIndicators({name: 'bulbs'})
	.addTo(controller);

	var groupingHeadline = new ScrollMagic.Scene( {
		triggerElement: '#groupingWrapper',
		triggerHook: 0,
		duration: '100%',
	})
	.setTween([
		
	])
	// .addIndicators({name: 'groupingHeadline'})
	.addTo(controller);

	var groupingList = new ScrollMagic.Scene( {
		triggerElement: '#groupingWrapper',
		triggerHook: 0, 
		duration: '70%',
		// offset: 50,
	})
	.setTween([
		TweenMax.from('.groupingList_item', 1, { opacity: 0}),
		TweenMax.from('.groupingList_item-left', 1, { x: -100}),
		TweenMax.from('.groupingList_item-right', 1, { x: 100}),
		TweenMax.to('.bulb-lit', {opacity: .3})
	])
	// .addIndicators({name: 'groupingList'})
	.addTo( controller );





// Music
// ------------------------------------------------------

	var musicTimeline = new TimelineMax ()
	.add([
		TweenMax.from('.musicVideo', 1, {y: 50, opacity: 0}),
		TweenMax.from('.music_content .headline-sub', 1, { y: 50, opacity: 0}),
		TweenMax.from('.musicList', 1, { delay: 1, y: 50, opacity: 0}),
	])

	var musiv = new ScrollMagic.Scene( {
		triggerElement: '#musicWrapper',
		triggerHook: .1,
		duration: 500,
	})
	.setTween([musicTimeline])
	.setPin('#musicWrapper')
	// .addIndicators({name: 'musicTimeline'})
	.addTo(controller)






// Bluetooth
// ------------------------------------------------------
	var blutoothTimeline = new TimelineMax ()
	.add([
		TweenMax.staggerFrom('.light_fixture', .25, {y: -200, opacity: 0}),
		TweenMax.from('.light_bulb', .25, {delay: .25, y: 200, opacity: 0}),
		TweenMax.from('.light_beam_container', .25, {delay: 1, opacity: 0}),
		TweenMax.from('.bluetooth_animation_signal', .25, {delay: .8, x: 200, opacity: 0}),
		TweenMax.from('.bluetooth_animation_logo', .4, {delay: .4, y: '100%', opacity: 0}),

	])

	var bluetoothScene = new ScrollMagic.Scene({
		triggerElement: "#bluetooth", 
		triggerHook: 0,
		duration: '100%', 
	})
		.setTween(blutoothTimeline)
		.setPin("#bluetooth")
		// .addIndicators({name: 'bluetoothScene'}) // add indicators (requires plugin)
		.addTo(controller);

	var animateBulbs = new ScrollMagic.Scene({
		triggerElement: '#bluetooth',
		offset: '700%'
	})
	.setClassToggle('.light_bulb', 'animate')
	// .addIndicators({name: 'bulbs' })
	.addTo(controller)

	var animateRays = new ScrollMagic.Scene({
		triggerElement: '#bluetooth',
		offset: '700%'
	})
	.setClassToggle('.light_beam', 'animate')
	// .addIndicators({name: 'Rays' })
	.addTo(controller)

// Timer
// _____________________________________________
	var timerTimeline = new TimelineMax ()
	.add([
		TweenMax.from('.timerVideo', 1, {y: 100, opacity: 0}),
		TweenMax.from('.timer_content .headline-sub', 1, {delay: 1, y: 100, opacity: 0}),
		TweenMax.staggerFrom('.timerList', 1, {delay: 1.5, y: 100, opacity: 0}),
	])

	var timer = new ScrollMagic.Scene( {
		triggerElement: '#timerWrapper',
		triggerHook: 0,
		duration: '100%',
		reverse: true,
	})
	.setTween(timerTimeline)
	.setPin('#timerWrapper')
	// .addIndicators({name: 'timerTimeline'})
	.addTo(controller)

});