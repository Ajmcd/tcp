function anchorScroll (this_obj, that_obj, base_speed) {
  var this_offset = this_obj.offset();
  var that_offset = that_obj.offset(); 
  var speed       = base_speed;
 
  $("html,body").animate({
    scrollTop: that_offset.top
  }, speed, 'swing', function() { });
}

var baseSpeed = 1500

$('.navBar_navList').on('click', 'a', function(e) {
	e.preventDefault();
	anchorScroll( $(this), $( $(this).attr("href") ), baseSpeed );
});

$('.scroll-btn').on('click', 'a', function(e) {
  e.preventDefault();
  anchorScroll( $(this), $( $(this).attr("href") ), baseSpeed );
});

$('.navBar_logoBlock').on('click', 'a', function(e) {
  e.preventDefault();
  anchorScroll( $(this), $( $(this).attr("href") ), baseSpeed );
});


var reverseGrouping = new Waypoint.Inview({
  element: $('#map-grouping')[0],
  exit: function(direction) {
    if(direction == 'down'){
      $('#map-grouping').addClass('goingUp');
      console.log('the element is above you now')  
    } else{
      $('#map-grouping').removeClass('goingUp');
    }
  }
});

var reverseMusic = new Waypoint.Inview({
  element: $('#map-music')[0],
  exit: function(direction) {
    if(direction == 'down'){
      $('#map-music').addClass('goingUp');
      console.log('the element is above you now')  
    } else{
      $('#map-music').removeClass('goingUp');
    }
  }
});

var reverseBluetooth = new Waypoint.Inview({
  element: $('#map-bluetooth')[0],
  exit: function(direction) {
    if(direction == 'down'){
      $('#map-bluetooth').addClass('goingUp');
      console.log('the element is above you now')  
    } else{ 
      $('#map-bluetooth').removeClass('goingUp')
    }
  }
});

var reverseTimer = new Waypoint.Inview({
  element: $('#map-timer')[0],
  exit: function(direction) {
    if(direction == 'down'){
      $('#map-timer').addClass('goingUp');
      console.log('the element is above you now')  
    } else{
      $('#map-timer').removeClass('goingUp')
    }
  }
});


