var musicVideoView = new Waypoint.Inview({
  element: $('#musicVideo')[0],
  enter: function(direction) {
    setTimeout(function(){
    	document.getElementById("musicVideo").play();
		}, 1000); 
    console.log("video enter view")
  },
  exited: function(direction) {
    document.getElementById("musicVideo").currentTime=0;
    console.log("video went " + direction)
  },
})

var timerVideoView = new Waypoint.Inview({
  element: $('#timerVideo')[0],
  entered: function(direction) {
    setTimeout(function(){
    	document.getElementById("timerVideo").play();
		}, 1000); 
    console.log("video enter view")
  },
  exited: function(direction) {
  	setTimeout(function(){
	    document.getElementById("timerVideo").currentTime=0;
    	console.log("video went " + direction)
  	}, 1000);
  },
})


