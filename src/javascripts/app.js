import waypoints from 'modules/waypoints.js'
console.log("waypoints loaded")

import './modules/scrollMagic.js'
console.log("scrollMagic loaded")

import './modules/navScroll.js'
console.log("navScroll loaded")

import './modules/playVideo.js'
console.log("playVideo loaded")

console.log(`app.js has loaded!`)
